#!/usr/bin/env python

from __future__ import print_function

import boto3
import json
from datetime import datetime


def get_asg_spot_instances():
    asg_client = boto3.client('autoscaling')
    ec2_resource = boto3.resource('ec2')

    spot_launch_configurations = []
    launch_configurations = asg_client.describe_launch_configurations()
    for lc in launch_configurations['LaunchConfigurations']:
        if 'SpotPrice' in lc:
            spot_launch_configurations.append(lc['LaunchConfigurationName'])

    spot_instance_obj = []
    spot_instances = []
    asg_groups = asg_client.describe_auto_scaling_groups()
    for asg in asg_groups['AutoScalingGroups']:
        if asg['LaunchConfigurationName'] in spot_launch_configurations and asg['DesiredCapacity'] != 0:
            for instance in asg['Instances']:
                spot_instance_obj.append(ec2_resource.Instance(instance['InstanceId']))
    for spot in spot_instance_obj:
        spot_instances.append(json.dumps({'type': spot.instance_type,
                                          'az': spot.placement['AvailabilityZone'],
                                          'platform': ('Windows' if spot.platform == 'Windows' else 'Linux/UNIX')})
                              )
    return spot_instances


def get_regular_spot_instances():
    ec2_client = boto3.client('ec2')

    spot_instances = []
    spot_instance_obj = ec2_client.describe_spot_instance_requests(
        Filters=[{'Name': 'state', 'Values': ['active'], 'Name': 'status-code', 'Values': ['fulfilled']}])
    for spot in spot_instance_obj['SpotInstanceRequests']:
        spot_instances.append(json.dumps({'type': spot['LaunchSpecification']['InstanceType'],
                                          'az': spot['LaunchedAvailabilityZone'],
                                          'platform': spot['ProductDescription']})
                              )
    return spot_instances


def handler(event, context):
    print('Loading function.')
    now = datetime.utcnow()
    ec2Client = boto3.client('ec2')
    cwClient = boto3.client('cloudwatch')

    instances = []
    instance_set = set(get_regular_spot_instances()) | set(get_asg_spot_instances())
    for j in instance_set:
        instances.append(json.loads(j))

    instance_types = []
    for i in instances:
        response = ec2Client.describe_spot_price_history(StartTime=now, EndTime=now,
                                                         InstanceTypes=[i['type']],
                                                         AvailabilityZone=i['az'],
                                                         ProductDescriptions=[i['platform']]
                                                         )
        if (i['type'] + "," + i['platform'] + "," + i["az"]) not in instance_types:
            instance_types.append(i['type'] + "," + i['platform'] + "," + i["az"])
            price_history = response['SpotPriceHistory']
            if price_history == []:
                response = ec2Client.describe_spot_price_history(StartTime=now, EndTime=now,
                                                                 InstanceTypes=[i['type']],
                                                                 AvailabilityZone=i['az'],
                                                                 ProductDescriptions=[i['platform'] + ' (Amazon VPC)'])
                price_history = response['SpotPriceHistory']
            try:
                cwClient.put_metric_data(Namespace='EC2 Spot InstanceType Pricing', MetricData=[
                    {'MetricName': 'Hourly Price',
                     'Dimensions': [{'Name': 'InstanceType', 'Value': price_history[0]['InstanceType']},
                                    {'Name': 'ProductDescription', 'Value': i['platform']},
                                    {'Name': 'AvailabilityZone', 'Value': i['az']}],
                     'Timestamp': now,
                     'Value': float(price_history[0]['SpotPrice'])}])
            except:
                print('Unable to put custom metric for InstanceType:', price_history[0]['InstanceType'])

    print('Function complete.')
    return 'complete'
